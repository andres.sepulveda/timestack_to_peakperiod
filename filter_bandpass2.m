function Hd = filter_bandpass2(Fs, Fc1)
%FILTER_BANDPASS Returns a discrete-time filter object.

%
% MATLAB Code
% Generated by MATLAB(R) 7.13 and the Signal Processing Toolbox 6.16.
%
% Generated on: 13-May-2014 19:06:20
%

% Butterworth Bandpass filter designed using FDESIGN.BANDPASS.

% All frequency values are in Hz.
% Modifyed by AOsorio: To include the samplin frequency variable

%Fs = 1.87;  % Sampling Frequency (For Bocagrande)

N   = 10;    % Order
% Fc1 = 1/30;  % First Cutoff Frequency 0.0333 (0.05)
Fc2 = 1/2;   % Second Cutoff Frequency 0.9 (0.5)

% Construct an FDESIGN object and call its BUTTER method.
h  = fdesign.bandpass('N,F3dB1,F3dB2', N, Fc1, Fc2, Fs);
Hd = design(h,'butter');

% [EOF]
